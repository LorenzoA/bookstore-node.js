const booksRouter = require('express').Router();
const Book = require('../models/book');
// const multery = require('../images/multer')

booksRouter.get('/', (req, res)=> {
  Book.find({},(err,books) => {
    if (err)throw err;
    res.render('books', {books:books});
  });
});

booksRouter.get("/new", (req, res) => {
  res.render("new");
});

booksRouter.post("/new",(req, res)=>{
  let newBook = new Book(req.body);
  newBook.save((err, book)=> {
    if (err) throw err;
    res.redirect('/books');
  })
});

booksRouter.route('/:book_id')
  .get((req, res)=>{
    Book.findById({_id: req.params.book_id}, (err, book) => {
      if (err) throw err;
      res.render("book", {book:book});
    })
})
booksRouter.get("/edit/:book_id", (req, res) => {
  Book.findById({_id: req.params.book_id}, (err, book) => {
  if (err) throw err;
    res.render('edit', {book:book});
  })
  })

booksRouter.post("/edit/:book_id", (req, res) => {
  Book.findById({_id: req.params.book_id}, (err, book) => {
  if (err) throw err;
  Object.assign(book, req.body).save((err,book) => {
    if (err) throw err;
    res.redirect('/books')
  });
  });
});
//.put((req, res)=>{
//  Book.findById({_id: req.params.book_id}, (err, book) => {
//    if (err) throw err;
//    Object.assign(book, req.body).save((err, book) => {
//      if (err) throw err;
//      res.json(book);
//    });
//  })
//})
// booksRouter.delete((req, res)=>{
//   Book.findById({_id: req.params.book_id}, (err) => {
//     if (err) throw err;
//     Object.assign(book, req.body).save((err,book) => {
//       if (err) throw err;
//     res.redirect('/books');
//   });
//   });
// });
booksRouter.get("/delete/:book_id",(req, res)=>{
  Book.remove({_id: req.params.book_id}, (err) => {
    
   
      if (err) throw err;
    res.redirect('/books');
  });
  });


module.exports = booksRouter;