const express = require('express');
const path = require("path");
const multer = require('multer');
const app = express();

const port = 4567;
const mongoose = require('mongoose');
const uri = "mongodb://localhost/book_db";
mongoose.connect(uri, {useNewUrlParser:true, useUnifiedTopology:true})
  .then(()=> console.log(`[MONGODB is connected <3]`))
  .catch(err => console.log(Error, err.message))


const booksRouter = require('./route/booksRoute');

app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", 'pug');
app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.get("/bonjour",(req,res)=>{
  res.send("Welcome you!")
})

app.use("/books", booksRouter)


app.listen(port, () =>{
  console.log('APPLICATION IS OK')
})