const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookSchema = new Schema ({
  title: {type: String},
  author: {type: String},
  date_published: {type: Date},
  pages: {type: Number, min:1},
  // images: {type: String}
});

const Book = mongoose.model('Book', bookSchema);

module.exports = Book;
